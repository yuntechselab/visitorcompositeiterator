package Composite;

import java.util.ArrayList;

import javax.swing.text.html.HTMLDocument.Iterator;

public class NonTerminalExpression extends AbstractExpression{
	
	ArrayList<AbstractExpression> abstractExpressionArray = new ArrayList<AbstractExpression>();

	public void addChild(AbstractExpression abstractExpression){
		abstractExpressionArray.add(abstractExpression);
	}
	
	
	@Override
	public void interpreter() {
		// TODO Auto-generated method stub
		java.util.Iterator<AbstractExpression> iterator = abstractExpressionArray.iterator();
		while(iterator.hasNext()){
			iterator.next().interpreter();
		}
	
	}
}
