package Composite;
public abstract class AbstractExpression{
	public  void addChild(AbstractExpression abstractExpression){}
	public  void remove(AbstractExpression abstractExpression){}
	public abstract void interpreter();
}